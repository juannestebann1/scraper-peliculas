const cheerio = require('cheerio');
const fs = require('fs');
const request = require('request');
const fetch = require('node-fetch');

var m =[];

function posting(obj){
  var body = obj;
  fetch('http://173.255.196.18:2828/movie/insert', { 
      method: 'POST',
      body:    JSON.stringify(body),
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
  })
  .then(res => res.json())
  .then(json => console.log(json));
}

function scraper(mv){
  let gene = [];
  let language = [];
  let obj = {};
  let link = [];
  var KT = false; 
  //console.log(mv);
  var d = mv.indexOf("https://pelislatino24.com/mipeli/");
  if (d === 0) {
    request(mv, (err, res, body) => {
    if (!err && res.statusCode == 200) {
      var $ = cheerio.load(body);

      let title = $('h1', 'div.data').html()
      let genero = $('div.sgeneros').html()
      let year = $('span.date').html()
      let cpg = $('span.rated').html()

      $('a', 'div.sgeneros').each(function () {
        var img = $(this).html();
        gene.push(img);});

      let cover = $('img', '.poster').attr('src')
      let synopsis = $('p', 'div.wp-content').html()

      $('a', 'td').each(function () {
        var l = $(this).attr('href')
        var ll = l.indexOf("https://pelislatino24.com/links/")
        if (ll !== -1) {
          link.push(l);
        }});

      obj.uploader = "JKing-Master";
      obj.size = "1.0GB";
      obj.title = title.slice(4, -3);
      obj.cover = cover;
      obj.clasification = cpg;
      obj.genre = gene;
      //console.log(year)
      if (year != null) {
        obj.year = year.slice(-4);
      }else{
        obj.year = 2018;
      }
      obj.synopsis = synopsis;
      //console.log(link);

      for (var i = 0; i < link.length; i++) {
          //console.log(link[i]);
          request(link[i], function(err, res, body){
              if (!err && res.statusCode == 200) {
                let $ = cheerio.load(body);
                var video = $('a', '.boton').attr('href')

                $('span.valor').each(function () {
                  var x =  $(this).html();
                  language.push(x);});

                var v = video.indexOf("openload")
                var v2 = video.indexOf("streamango")
                var v3 = video.indexOf("rapidvideo")

                //console.log(v+' | '+v2+' | '+v3+' ')

                if (v > -1 && v2 === -1 && v3 === -1 && KT === false) {
                  //console.log('validate')
                  KT = true;
                  obj.key = video;
                  obj.language = language[1];
                  obj.openload = false;
                  obj.rapidvideo = false;
                  obj.streamango = false;
                  if(obj.key.includes("openload")) {
                      obj.openload = true;
                  } else if (obj.key.includes("rapidvideo")) {
                      obj.rapidvideo = true;
                  } else if (obj.key.includes("streamango")) {
                      obj.streamango = true;
                  }
                  posting(obj);
                  console.log('\x1b[33m', '[DONE]' ,'\x1b[0m','\x1b[0m', obj.title ,'\x1b[0m');}
                if (v === -1 && v2 > -1 && v3 === -1 && KT === false) {
                  //console.log('validate')
                  KT = true;
                  obj.key = video;
                  obj.language = language[1];
                  obj.openload = false;
                  obj.rapidvideo = false;
                  obj.streamango = false;
                  if(obj.key.includes("openload")) {
                      obj.openload = true;
                  } else if (obj.key.includes("rapidvideo")) {
                      obj.rapidvideo = true;
                  } else if (obj.key.includes("streamango")) {
                      obj.streamango = true;
                  }
                  posting(obj);
                  console.log('\x1b[33m', '[DONE]' ,'\x1b[0m','\x1b[0m', obj.title ,'\x1b[0m');}
                if (v === -1 && v2 === -1 && v3 > -1 && KT === false) {
                  //console.log('validate')
                  KT = true;
                  obj.key = video;
                  obj.language = language[1];
                  obj.openload = false;
                  obj.rapidvideo = false;
                  obj.streamango = false;
                  if(obj.key.includes("openload")) {
                      obj.openload = true;
                  } else if (obj.key.includes("rapidvideo")) {
                      obj.rapidvideo = true;
                  } else if (obj.key.includes("streamango")) {
                      obj.streamango = true;
                  }
                  posting(obj);
                  console.log('\x1b[33m', '[DONE]' ,'\x1b[0m','\x1b[0m', obj.title ,'\x1b[0m');}
                if (v === -1 && v2 === -1 && v3 === -1 && KT === false){
                  //console.log('invalidate')
                  KT = false;
                  obj.key = video;
                  console.log('\x1b[31m', '[FAIL]' ,'\x1b[0m','\x1b[0m', obj.title ,'\x1b[0m')}}
          })}}
    })}
  }

function list(arg='accion', n=1) {
    var x = 0;
    request("https://pelislatino24.com/genero/"+arg+"/page/"+n, (err, res, body) => {
      if (!err && res.statusCode == 200) {
        var $ = cheerio.load(body);
        $('a', 'h3').each(function () {
          var l = $(this).attr('href');
            if (l.indexOf('https://pelislatino24.com/genero/') != -1) {
            }else{
              console.log('[DONE] '+l);
              x++
              fs.appendFile('mynewfile1.txt', l+",", function (err) {
                if (err) throw err;
              });
            }
        });
      }else{
        console.log('[page not found] total inserted 0');
      }
    });}

if (process.argv[2] === 'find') {
  if (process.argv.length > 3) {
    if (process.argv.length > 4) {
      a = process.argv[4] || 1;
      for (var m = 1; m < a; m++) {
        list(process.argv[3], m);
      }
    }
  }else{
    //list();
    console.log('You must provide a <category> in which to search:')
    console.log(' accion, animacion, aventura,\n ciencia-ficcion, comedia, crimen,\n drama, familia, fantasia,\n guerra, historia, romance,\n terror\n')
    console.log('ejm: npm start find accion')
    console.log('ejm: npm start find accion')
  }
}

if (process.argv['2'] === 'rasp') {
  var str = fs.readFileSync('mynewfile1.txt', 'utf8');
  array = str.slice(0, -1)
  m = array.split(",");
  m.forEach(function(element) {
    scraper(element);
  });
}